import { Component, OnInit } from '@angular/core';
import {FormControl,FormGroup,FormBuilder,Validators} from '@angular/forms'
import {CallapiService} from '../../app/callapi.service'

import { Router } from "@angular/router";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username = new FormControl('',[Validators.required]);
  password = new FormControl('',[Validators.required]);
  
  private failed = false

  constructor(private callapi: CallapiService, private route:Router) { }

  ngOnInit() {
  }

  Login(){
   // alert(this.username.value+ ' '+this.password.value)
    this.callapi.Login(this.username.value,this.password.value).subscribe( (res) => {
      if(res == 200 || res == 302){
        this.route.navigate(['/home']).then( (e) => {
          if (e) {
            console.log("Navigation is successful!");
          } else {
            console.log("Navigation has failed!");
          }
        });
      }else{
        this.failed = true; 
      }
    });
      
  }

}
