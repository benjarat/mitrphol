import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders ,HttpParams} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CallapiService {
  apiURL: string = 'https://mitrpholapi.herokuapp.com';

  constructor(private httpClient:HttpClient) { }

  
 

  Login(username, password): Observable<any> {
    const body = {
      username:username,
      password:password
    }

   
    return this.httpClient.get<any[]>(`${this.apiURL}/login/${username}/${password}`,{headers: new HttpHeaders({ "Accept": "application/json" })
    
    })
  }
  
}







const handel = {
  handleError(error) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  window.alert(errorMessage);
  return throwError(errorMessage);
}
}
